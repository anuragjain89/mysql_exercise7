DROP DATABASE IF EXISTS bank;
CREATE DATABASE bank;
USE bank;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT AUTO_INCREMENT,
  name VARCHAR(20),
  email VARCHAR(20),
  account_no INT,
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts (
  id INT AUTO_INCREMENT,
  account_no INT,
  balance DECIMAL(10,2),
  PRIMARY KEY(id),
  UNIQUE INDEX(account_no)
);

INSERT INTO users(name, email, account_no) VALUES ('userA', 'userA@vinsol.com', 1);
INSERT INTO users(name, email, account_no) VALUES ('userB', 'userB@vinsol.com', 2);
INSERT INTO users(name, email, account_no) VALUES ('userC', 'userC@vinsol.com', 3);
INSERT INTO users(name, email, account_no) VALUES ('userD', 'userD@vinsol.com', 4);

INSERT INTO accounts(account_no, balance) VALUES (1, 10000.00);
INSERT INTO accounts(account_no, balance) VALUES (2, 20000.00);
INSERT INTO accounts(account_no, balance) VALUES (3, 30000.00);
INSERT INTO accounts(account_no, balance) VALUES (4, 40000.00);

SELECT * FROM users;
SELECT * FROM accounts;

SET AUTOCOMMIT=0;

LOCK TABLES accounts WRITE, users READ;
UPDATE accounts SET balance = balance + 1000 WHERE account_no = (SELECT account_no FROM users WHERE name = 'userA');
SELECT * FROM accounts;
SAVEPOINT amountDeposit;

UPDATE accounts SET balance = balance - 500 WHERE account_no = (SELECT account_no FROM users WHERE name = 'userA');
SELECT * FROM accounts;
UNLOCK TABLES;

-- COMMIT;

LOCK TABLES accounts WRITE, users READ;

UPDATE accounts SET balance = balance - 200 WHERE account_no = (SELECT account_no FROM users WHERE name = 'userA');
UPDATE accounts SET balance = balance + 200 WHERE account_no = (SELECT account_no FROM users WHERE name = 'userB');
SELECT * FROM accounts;
UNLOCK TABLES;

-- COMMIT;